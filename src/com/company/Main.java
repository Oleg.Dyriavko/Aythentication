package com.company;


import java.util.Base64;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {

      //  System.out.println(new String(Base64.getEncoder().encode("qwerty".getBytes())));

        Scanner scanner = new Scanner(System.in);

        CheckUser checkUser = new CheckUser();

        System.out.println("Enter your name:");
        String login = scanner.next();

        System.out.println("Enter your password");
        String password = scanner.next();

        User user = new User(login, password );

        checkUser.encrypt(user);

        if (checkUser.checkPassword(user)){
            System.out.println("SUCCESSFUL");
        }else {
            System.out.println("FAILED");
        }
    }
}
